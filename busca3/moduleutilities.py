# -*- coding: utf-8 -*-

import importlib        as importlib
import importlib.util

#import json             as json
from busca3 import json1
#import json1            as json1

import sys              as sys

# configure logging
import logging
# define busca logger as the generic logger
lg=logging

# functions
def find_module_or_exit (spec):
    # spec can be a string - e.g., "numpy"
    if importlib.util.find_spec(spec): # returns a ModuleSpec object if module found
        pass 
    else:
        msg = json1.json_msg_module_not_accessible(spec)
        lg.error(msg)
        lg.info(json1.json_last_log_msg())
        sys.exit(msg)
