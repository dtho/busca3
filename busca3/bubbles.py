# -*- coding: utf-8 -*-

# (c) David A. Thompson <thompdump@gmail.com>
#
# This file is part of Busca
#
# Busca is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Busca is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Busca. If not, see <http://www.gnu.org/licenses/>.


#
# bubbles.py
#
#   1. accepts an input file path as CLI argument
#      - this should be a deskewed png image where the calibration and bubble marks are (in theory) located where they are "supposed to be"
#   2. determines the filled/empty status of bubble marks
#   3. outputs the filled/empty data to stdout
#
import importlib
import math

from busca3 import moduleutilities
#import moduleutilities as moduleutilities

import numpy
import os.path
import sys
import tempfile

from busca3 import barscan
from busca3 import json1
#import barScan
#import json1

# configure logging
import logging
# define deskew logger
lg=logging

moduleutilities.find_module_or_exit("cv2")
import cv2

# internal/busca modules
from busca3 import feature_detect
from busca3 import json1
from busca3 import reference_image
from busca3 import util

#
# exceptions
#
class LocateBubbleError(Exception):
    '''Raise when fail to locate bubble(s) at anticipated position(s)'''
    def __init__(self, message, files, *args):
        self.message = message # without this you may get DeprecationWarning
        self.files = files
        # allow users initialize misc. arguments as any other builtin Error
        super(LocateBubbleError, self).__init__(message, file, *args)

#
# functions
#
def bubble_coordinates_from_file(xyfile):
    """Return bubble coordinates for an image as an array of arrays of arrays. Each outer subarray represents a page. Each inner subarray represents a row. Each member of a row subarray is a list/tuple representing an xy coordinate corresponding to a bubble location. XYFILE is a string representing the path for a file."""
    pages = []
    rows = []
    try:
        with open(xyfile) as f:
            # get rows for page
            # - if a number, it is bubble_x_cm and now grab bubble_y_cm
            # - if newline, we are at the start of a new page
            # - if a space, push to next item
            for line in f:
                line = line.split()
                if line:
                    row = [float(i) for i in line]
                    rows.append(row)
                else:
                    pages.append(rows)
                    rows = []
        # If rows is populated but pages is empty, the XY file may
        # have issues.
        if (len(rows) > 0 and not len(pages) > 0):
            lg.warn(f'The XY file, {xyfile}, should be double-checked.')
        return pages
    except IOError as e:
        lg.error("Issue with xy file %s: %s",xyfile,e)

def bubble_coordinates_as_px(pages_cm, px_per_cm):
    pages_px = []
    for page in pages_cm:
        #print page
        page_px = []
        for row in page:
            row_px = row_cm_to_row_px(row, px_per_cm, px_per_cm)
            page_px.append(row_px)
        pages_px.append(page_px)
    return pages_px

# given list of xy coordinates in cm, return list of tuples in px
# COORDINATES_CM has the form [x1,y1,x2,y2,... xn,yn]
# PX_PER_CM is a number
#    example: [1.75, -14.599999, 2.5, -14.599999, 3.25, -14.599999, 4.0, -14.599999, 4.75, -14.599999]
def convert_coordinates_cm_to_px(coordinates_cm, px_per_cm):
    coordinates_px = []
    i = 0
    while (i < len(coordinates_cm)):
        x_cm = coordinates_cm[i]
        y_cm = coordinates_cm[i+1]
        x_px = x_cm * px_per_cm
        # see doc/xy-file-format.txt for reason y coordinates are negative
        y_px = -1 * y_cm * px_per_cm
        i=i+2
        coordinates_px.append( (x_px, y_px) )
    return coordinates_px

def look_for_circles_in_row(bubble_imgray, x1, y1, x2, y2,
                            min_radius_px, max_radius_px, min_dist, diag_img):
    """Return False if one or more circles don't appear to be present. Return True if all circles appear to be present."""
    # find_circles
    # - each member of CIRCLES is a tuple of the form [x y radius] (example: [265.5,587.5,15.57241154]) and dtype float32
    circles = feature_detect.find_circles(bubble_imgray,min_radius_px,max_radius_px,min_dist)
    debug_p = False
    # development/diagnostic: note location of each circle as it is detected (put 80% radius red dot @ first circle found)
    if len(circles) == 1:
        circle = circles[0]
        circle_center_x = circle[0]+x1
        circle_center_y = circle[1]+y1
        # FIXME: write and use draw_circle_safe and draw_rect_safe -- functions which do sanity checks on inputs, warn if they don't look good, but then try to coerce before promptly giving up
        circle_center_x = int(circle_center_x)
        circle_center_y = int(circle_center_y)
        circle_r = 0.8*circle[2]
        circle_r = int(circle_r)
        rect_pt1 = (int(circle_center_x-5),int(circle_center_y-5))
        rect_pt2 = (int(circle_center_x+5),int(circle_center_y+5))
        grayscale_white = 0
        line_thickness = 3
        cv2.rectangle(diag_img, rect_pt1, rect_pt2, 255, line_thickness)
        cv2.circle(diag_img, (circle_center_x, circle_center_y), circle_r, grayscale_white, line_thickness)
        return (True,None,None)
    # if bubble not detected, this is a real issue!
    # - log event
    # - draw rectangle where circle was not found
    elif ( len(circles) == 0 ):
        files = [ ]
        if debug_p:
            # write out diagnostic image file
            # is rectangle filled? empty? (assume empty)
            cv2.rectangle(diag_img,
                          (int(x1),int(y1)),
                          (int(x2),int(y2)),
                          20,
                          2)
            # write complete image
            outputFile = tempfile.mktemp(".png", "bubblefail", "/tmp/")
            cv2.imwrite(outputFile,diag_img)
            # write only problematic rectangle
            rectFile = tempfile.mktemp(".png", "bubblefail", "/tmp/")
            cv2.imwrite(rectFile, bubble_imgray)
            files = [outputFile, rectFile]
        # development only *** END ***
        return (False,
                files,
                [(int(x1),int(y1)), (int(x2),int(y2))]  # area of interest 
                )

def pixels_in_square(x, y, square_dimension_px, img):
    pixels = []
    start_y = int(math.ceil(y - square_dimension_px/2))
    end_y = int(math.floor(y + square_dimension_px/2)) - 1
    start_x = int(math.ceil(x - square_dimension_px/2))
    end_x = int(math.floor(x + square_dimension_px/2)) - 1
    for y in range(start_y, end_y):
        for x in range(start_x, end_x):
            pixels.append(img[y, x])
    return pixels

def row_cm_to_row_px(row, x_px_per_cm, y_px_per_cm):
    """Convert array with series of numbers representing xy coordinate
    pairs in cm to array with pixel values."""
    row_cm = []
    row_length = len(row)
    i = 0
    while i < row_length:
        x_cm = row[i]
        y_cm = row[i+1]
        i = i+2
        bubble_x_px = x_cm * x_px_per_cm # cols_per_cm
        bubble_y_px = y_cm * y_px_per_cm # rows_per_cm
        coordinate = (bubble_x_px, bubble_y_px)
        row_cm.append(coordinate)
    return row_cm

# X: x coordinate in ( px? cm? )
# Y: y coordinate in ( px? cm? )
def score_bubble(x, y, img, bubble_radius_px):
    "Return either 1 or 0 depending on whether bubble is filled in or not, respectively."
    # conservative grab region (possibilities: (A) use circle at 80% of radius (B) use square in circle where square side has length 2x and 2x^2 = r^2 --> x = sqrt(r^2/2))
    # (A) reduced circle:
    # scoring_radius_px = 0.8*bubble_radius_px
    # (B) circumscribed square:
    square_dimension_px = 2*bubble_radius_px/math.sqrt(2)
    # grab all pixels within grab region
    pixels = pixels_in_square(x, y, square_dimension_px, img)
    # score pixels
    pixel_scores = score_bubble_pixels(pixels)
    # sanity check
    if (not pixel_scores):
        raise Exception('pixel scoring failed')
    pixel_score_mean = numpy.mean(pixel_scores)
    if pixel_score_mean > 0.8:
        return 1
    else:
        return 0

def score_bubble_pixel(pixel):
    if ( isinstance(pixel, int)):
        raise Exception('only greyscale supported')
    # numpy.uint8 range is 0-255
    max = None
    if ( type(pixel) is numpy.uint8 ):
        max = 255
    # sanity check
    if not max:
        raise Exception('integer type not recognized (yet...)')
    threshold = 0.8 * 255
    if (pixel<threshold):
        return 1
    else:
        return 0

def score_bubbles (bubble_coordinates_cm,image_file,page_n,debugp=False):
    """
    BUBBLE_COORDINATES_CM is an array for a single page.
    IMAGE_FILE is a string representing a filesystem path.
    PAGE_N is optional argument specifying page number.
    """
    lg.debug("score_bubbles %s %s",bubble_coordinates_cm,image_file)
    debug_p = True              # False
    # imread loads image from file, as a matrix (numpy.ndarray)
    # IMGRAY: a matrix (numpy.ndarray) (e.g., as generated by imread)
    imgray = util.sane_imread(image_file, cv2.IMREAD_GRAYSCALE)
    # constants
    px_per_cm = util.px_per_cm(imgray,
                               reference_image.ref_img_x_cm,
                               reference_image.ref_img_y_cm)
    bubble_radius_px = reference_image.bubble_radius_cm * px_per_cm
    rows_scores = []
    # diagnostic/development: mark locations on copy of image
    diag_img = numpy.copy(imgray)
    # ROW is a list of tuples representing xy coordinates of bubbles
    # example: [(1.75, -21.6), (2.5, -21.6), (3.25, -21.6), (4.0, -21.6), (4.75, -21.6)]
    missing_bubbles_p = False
    diagnosticFiles = []
    for row_cm in bubble_coordinates_cm:
        row_px = convert_coordinates_cm_to_px(row_cm,px_per_cm)
        row_scores = None
        # catch exceptions here so we can continue to look for other missing bubbles and give user a complete picture of what's wrong by writing out diag_img below...
        try:
            row_scores,row_missing_bubbles_p,rowDiagnosticFiles = score_bubbles_row(row_px,imgray,diag_img,bubble_radius_px,page_n)
            if row_missing_bubbles_p:
                missing_bubbles_p = True
                diagnosticFiles=diagnosticFiles+rowDiagnosticFiles
        # FIXME: this isn't a good idea to catch all exceptions here -- it actually hinders debugging if there are coding issues in score_bubbles_row --> catch specific exceptions, not everything --> try and kludge for this by including debug (below) to describe exception
        except LocateBubbleError as e:
            diagnosticFiles=diagnosticFiles+e.files
            missing_bubbles_p = True
        except Exception as e:
            missing_bubbles_p = True
        rows_scores.append(row_scores)
    # check that diag_img is really being written
    cv2.line(diag_img,(0,0),(511,511),
             100 # FIXME: 255 at any position causes color to show up as white! --> why? A: for grayscale, color can be a single number (not a tuple)
             ,5)
    if debugp:
        # write out diagnostic image file
        output_file_suffix='png'
        input_file_sans_suffix, input_file_suffix = os.path.splitext(image_file)
        outputFile = input_file_sans_suffix + "BUBBLEDIAG" + "." + output_file_suffix
        # imwrite chooses format based on filename extension
        cv2.imwrite(outputFile,diag_img)
        if (missing_bubbles_p == True ):
            diagnosticFiles.append(outputFile)
            lg.warn(json1.json_msg_bubbles_not_found(outputFile,None))
            # - see notes in bubble-detection.txt
            # this should be caught by busca/busca.py
            #raise LocateBubbleError('Unable to locate bubble(s) at anticipated location(s)',[outputFile,rowDiagOutputFile]);
    return rows_scores,missing_bubbles_p,diagnosticFiles

# ROW is a list of tuples representing xy coordinates of bubbles in pixels (x relative to left-hand side; y relative to the top and increasing as the position descends down the page)

# Return row_scores, a [ list? sequence? array? of integers? x? ]
def score_bubbles_row(row,imgray,diag_img,bubble_radius_px,page_n):
    """
    ROW is [ ?? ]

    Return multiple values (as a tuple): the scores for row ROW, a
    boolean indicating whether there was a failure to detect a bubble
    at any point (True corresponds to failure to detect a bubble at
    some point), and (if the second value is True), a list of
    diagnostic files associated with the failure to detect a bubble.
    DIAG_IMG is a grayscale image.
    """
    # configure diagnostics/debugging
    diag_config = {
        'circles_p': False,
        'line_p': True,
    }
    # note if a row is encountered where we can't see bubbles
    circles_detected_p = True
    diagnosticFiles=[]
    row_scores = []
    row_length = len(row)
    i=0
    # see deskew.py for similar circle detection code
    px_per_cm = util.px_per_cm(imgray,reference_image.ref_img_x_cm,reference_image.ref_img_y_cm)
    min_radius_cm = 0.7*reference_image.bubble_radius_cm
    max_radius_cm = 1.8*reference_image.bubble_radius_cm
    min_radius_px = min_radius_cm * px_per_cm
    max_radius_px = max_radius_cm * px_per_cm
    # looking for a single circle; search rectangle should be less than 1 cm with either dimension
    min_dist_cm = 1
    min_dist = min_dist_cm * px_per_cm
    while (i<row_length):
        # xy_tuple specifies circle center
        xy_tuple = row[i]
        x = xy_tuple[0]
        y = xy_tuple[1]
        i = i+1
        # define rectangle encompassing putative location of bubble
        x1 = int(round(x - max_radius_px))
        x2 = int(round(x + max_radius_px))
        y1 = int(round(y - max_radius_px))
        y2 = int(round(y + max_radius_px))
        if diag_config['line_p']:
            # add 'x' at position for diagnostic/debug purposes
            cv2.line(diag_img,(x1,y1),(x2,y2),20,2)
            cv2.line(diag_img,(x1,y2),(x2,y1),20,2)
        bubble_imgray = imgray[y1:y2,x1:x2]
        # attempt to find circle -- use cv2 feature detection to look for circles and then perform sanity checks
        row_circles_detected_p,files,area = look_for_circles_in_row(bubble_imgray,x1,y1,x2,y2,min_radius_px,max_radius_px,min_dist,diag_img)
        if not row_circles_detected_p:
            # Options:
            # 1. lg.error(json1.json_msg_bubble_not_found(...
            #    --> need all the data for the log message
            # 2. signal error/condition and pass on what we've got...
            lg.error(json1.json_msg_bubble_not_found(
                files,
                'min_radius_px: {}, max_radius_px: {}, min_dist: {}'.format(min_radius_px, max_radius_px, min_dist),
                area,
                (diag_img.shape[1],diag_img.shape[0]),            # x,y dimensions of diag_img
                page_n          # page_n
            )) 
            circles_detected_p = False
            diagnosticFiles = diagnosticFiles + files
        ##############################################
        # diagnostic/development
        # - draw circle at bubble under evaluation
        if diag_config['circles_p']:
            cv2.circle(diag_img,
                       (int(x),int(y)),
                       10,
                       150,
                       -3
                       )
        score = score_bubble(x,y,imgray,bubble_radius_px)
        row_scores.append(score)
    # see notes in bubble-detection.txt -- at this point, we can
    # 1. [ current implementation ] at the minimum, at least note in the scanset, as a warning, that the bubble detection algorithm failed to locate one or more circles --> return circles_detected_p as a second value and let calling function handle this
    # 2. assume any further analysis is a waste of time and communicate to calling function that there isn't much point in processing image further
    #if ( not circles_detected_p ):
    #    raise LocateBubbleError('Unable to locate bubble(s) at anticipated location(s)',files);
    return row_scores, not circles_detected_p, diagnosticFiles

def score_bubble_pixels(pixels):
    pixel_scores = []
    for pixel in pixels:
        score = score_bubble_pixel(pixel)
        pixel_scores.append(score)
    return pixel_scores

def score_page(image_file,bubble_coordinates_cm,page_n,debugp=False):
    """
    The image itself is referenced by IMAGE_FILE. Known physical
dimensions of image are specified by the globals REF_IMG_Y_CM = 27.94
AND REF_IMG_X_CM = 21.59). BUBBLE_COORDINATES_CM specifies the known
physical coordinates of bubbles. PAGE_N is specifies the page number.
    """
    # determine filled/empty status of each circle
    full_empty_bubbles,missing_bubbles_p,diagnosticFiles = score_bubbles(bubble_coordinates_cm,image_file,page_n,debugp=debugp)
    return full_empty_bubbles,missing_bubbles_p,diagnosticFiles

def score_pages(image_files, bubble_coordinates_file,debugp=False):
    """
    IMAGE_FILES is a list of strings corresponding to
    files with image data for the pages under analysis.
    BUBBLE_COORDINATES_FILE is the file holding coordinate data for
    all pages. Return a tuple where the first member is an array
    representing scores for all bubbles on all pages.
    """
    lg.debug("score_pages image_files %s",image_files)
    # CIRCLE_COORDINATES_CM: anticipated circle coordinates
    circle_coordinates_cm = bubble_coordinates_from_file(bubble_coordinates_file)
    i = 0                       # index in IMAGE_FILES
    i_max = len(image_files)
    bubble_coordinates_pages_n = len(circle_coordinates_cm)
    # sanity check
    if ( not bubble_coordinates_pages_n >= i_max ):
        raise Exception('bubble coordinates file does not contain data for all image files')
    bubble_detection_failed_p=False
    diagnosticFiles=[]
    page_scores = [] 
    while (i<i_max):
        image_file = image_files[i]
        scores,bubble_detection_failed_on_page_p,pageDiagnosticFiles = score_page(image_file,circle_coordinates_cm[i],i+1,debugp=debugp)
        if bubble_detection_failed_on_page_p:
            bubble_detection_failed_p = True
            diagnosticFiles=diagnosticFiles+pageDiagnosticFiles
        page_scores.append(scores)
        i = i+1
    return page_scores,bubble_detection_failed_p,diagnosticFiles

def barcodes_on_pages(image_files):
    """IMAGE_FILES is an ordered list of strings corresponding to files with image data for the pages under analysis. Return an array representing strings corresponding to a barcode scan for each image file."""
    i = 0                       # index in IMAGE_FILES
    i_max = len(image_files)
    barcodes = []
    while (i<i_max):
        image_file = image_files[i]
        # extract barcode for each page
        barcodes.append(barscan.barcodeScan(image_file,None))
        i = i+1
    return barcodes

# utility function to infer # of pages per test using xy file content
def xy_to_page_number(bubble_coordinates_file):
    """BUBBLE_COORDINATES_FILE is the file holding coordinate data for all pages. Return an integer."""
    # sanity check(s)
    util.assert_file_exists_p(bubble_coordinates_file)
    circle_coordinates_cm = bubble_coordinates_from_file(bubble_coordinates_file)
    bubble_coordinates_pages_n = len(circle_coordinates_cm)
    return bubble_coordinates_pages_n
