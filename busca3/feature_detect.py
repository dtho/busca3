# -*- coding: utf-8 -*-

# (c) David A. Thompson <thompdump@gmail.com>
#
# This file is part of Busca
#
# Busca is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Busca is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Busca. If not, see <http://www.gnu.org/licenses/>.

import cv2
import math
import numpy
import tempfile

# configure logging
import logging
# define busca logger as the generic logger
lg=logging

# internal/busca modules
from busca3 import util


# debugging/development only
find_circles_debug_img = None

def find_circles(img,min_r,max_r,min_dist,votesThreshold=13):
    """IMG is a grayscale image. MIN_R is the minimum radius of interest. MAX_R is the maximum radius of interest. Return a list where each member has the form [ a b c] ."""
    lg.debug("FIND_CIRCLES.00 img: %s",img)
    # debugging/development only
    global find_circles_debug_img
    find_circles_debug_img = img
    #
    # debugging finding calibration circles
    #cv2.HoughCircles(deskew.find_circles_debug_img, cv2.cv.CV_HOUGH_GRADIENT, 1, 988, param1=50, param2=17, minRadius=23, maxRadius=36)

    # FIXME: we fudge here -- good idea? or should we rigorously require integers for radii limits?
    # non-integer values are nonsensical for upper/lower bounds for radii
    min_r = int(math.floor(min_r))
    max_r = int(math.ceil(max_r))
    lg.debug("min_r: %s",min_r)
    # HoughCircles finds circles in a grayscale image
    #   - only one method currently implemented
    #   - returns an output vector of the found circles
    method=cv2.HOUGH_GRADIENT # OLD: cv2.cv.CV_HOUGH_GRADIENT
    min_dist=int(min_dist)
    # circles: a numpy ndarray with shape (1,N,3) where each member is a tuple of the form [x y radius] (example: [265.5,587.5,15.57241154]) and dtype float32
    # example:
    #   [[[ 1215.5         1501.5           23.71708298]
    #     [ 1215.5           59.5           24.54587555]
    #     [   58.5         1501.5           23.71708298]
    #     [   58.5           58.5           23.92697144]]]


    # see 'bubble-detection.txt' and 'calibration-marks--notes.txt' for more on param2 setting and approach used to identify circles
    #   -- currently: we use the entire page
    #   -- if things are problematic in the future, probably best to ditch the whole page and just focus on corners where we know the circles of interest are...
    if votesThreshold:
        parameter_2 = votesThreshold
    else:
        parameter_2 = 13
    circles = cv2.HoughCircles(img,
                               method, 1,
                               min_dist,
                               param1=50,
                               param2=parameter_2,
                               minRadius=min_r,
                               maxRadius=max_r)
    lg.debug("parameter_2: %s \n circles: %s",parameter_2, circles)
    if circles is None:
        return []
    else:
        # uint16 converts precise floating-point positions to integer values
        circles = numpy.uint16(numpy.around(circles))
        lg.debug("FIND_CIRCLES.90")
        # convert to a simple sequence of circles
        circles_list = circles[0]
        return circles_list


# if a circle is in a corner
# - it is NOT in the middle
# -> define the middle
# -> only look outside of the middle: inspect 'corners' defined by: top 30%, bottom 30%, left 25%, right 25%
#
# ┏━━━━━━┓
# ┃      ┃
# ┃ ┘  └ ┃ <- top_corner_y
# ┃      ┃
# ┃ ┐  ┌ ┃ <- bottom_corner_y
# ┃      ┃
# ┗━━━━━━┛
#
# - could refine things by starting with looser parameters and gradually tightening until only have one circle

def find_circles_in_corners01 (img,min_radius_px,max_radius_px,min_dist,debug_file_name,debug_file_suffix):
    lg.debug("FIND_CIRCLES_IN_CORNERS01")
    debug_p = False
    img_rows = img.shape[0]
    img_cols = img.shape[1]
    # note: 0.15 and 0.2 margins were pulled from a hat (they may need some refining...)
    top_corner_y = int(img_rows*0.15)
    bottom_corner_y = int(img_rows*0.85)
    left_corner_x = int(img_cols*0.20)
    right_corner_x = int(img_cols*0.80)
    # define corner images
    ul_corner_img=img[0:top_corner_y,0:left_corner_x]
    ur_corner_img=img[0:top_corner_y,right_corner_x:img_cols]
    bl_corner_img=img[bottom_corner_y:img_rows,0:left_corner_x]
    br_corner_img=img[bottom_corner_y:img_rows,right_corner_x:img_cols]
    ul_circles=find_circles(ul_corner_img,
                            min_radius_px,
                            max_radius_px,
                            min_dist)
    ur_circles=find_circles(ur_corner_img,min_radius_px,max_radius_px,min_dist)
    bl_circles=find_circles(bl_corner_img,min_radius_px,max_radius_px,min_dist)
    br_circles=find_circles(br_corner_img,min_radius_px,max_radius_px,min_dist)
    lg.debug("FIND_CIRCLES_IN_CORNERS01.70: ul_circles: %s %s %s",ul_circles,ul_corner_img,util.draw_circles_2)
    util.draw_circles_2(ul_corner_img, ul_circles,radius=20)
    lg.debug("FIND_CIRCLES_IN_CORNERS01.73: %s %s %s",debug_file_name,debug_file_suffix,ul_corner_img)
    lg.debug("ur_circles: %s",ur_circles)
    util.draw_circles_2(ur_corner_img,ur_circles,radius=20)
    lg.debug("bl_circles: %s",bl_circles)
    util.draw_circles_2(bl_corner_img,bl_circles,radius=20)
    lg.debug("br_circles: %s",br_circles)
    util.draw_circles_2(br_corner_img,br_circles,radius=20)
    # FIXME: use a *single* diagnostic/debug output image for all debug/diagnostic output (see below)
    if debug_p:
        cv2.imwrite(debug_file_name+"ul."+debug_file_suffix,ul_corner_img)
        cv2.imwrite(debug_file_name+"ur."+debug_file_suffix,ur_corner_img)
        cv2.imwrite(debug_file_name+"bl."+debug_file_suffix,bl_corner_img)
        cv2.imwrite(debug_file_name+"br."+debug_file_suffix,br_corner_img)
    #
    # return circles adjusted to original locations in IMG
    # - ul_circles doesn't need adjustment
    lg.debug("ur_circles[0][0]: %s",ur_circles)
    ur_circles[0][0]=right_corner_x+ur_circles[0][0]   # adjust x
    #ur_circles[0][1]=   # adjust y
    #bl_circles[0][0]=right_corner_x+ur_circles[0][0]   # adjust x
    bl_circles[0][1]=bottom_corner_y+bl_circles[0][1]   # adjust y
    br_circles[0][0]=right_corner_x+br_circles[0][0]   # adjust x
    br_circles[0][1]=bottom_corner_y+br_circles[0][1]   # adjust y
    # return array must be a numpy.ndarray object
    circles=numpy.array([ul_circles[0],ur_circles[0],bl_circles[0],br_circles[0]])
    lg.debug("FIND_CIRCLES_IN_CORNERS01.F0: circles: %s", circles)
    return circles
