# -*- coding: utf-8 -*-

# (c) David A. Thompson <thompdump@gmail.com>
#
# This file is part of Busca
#
# Busca is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Busca is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Busca. If not, see <http://www.gnu.org/licenses/>.

import cv2
import numpy    as np
import os
import os.path

# configure logging
import logging
# define busca logger as the generic logger
lg=logging

#
# files, directories, paths, executables, ...

# see http://stackoverflow.com/questions/377017/test-if-executable-exists-in-python
# Python 3.3 offers shutil.which()
def which(program):
    import os
    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)
    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file
    return None

# sdefine a uniform approach to testing for existence of a file
def assert_file_exists_p(path):
    if file_exists_p(path):
        return True
    else:
        raise AssertionError("file " + path + " does not exist")

def file_exists_p(path):
    return os.path.isfile(path)

#
# circles

# circles: a list of of numpy ndarray circles
def atCircles(img, circles, fn):
    for circle in circles:
        if numpyCircleP(circle):
            fn(circle)
        else:
            raise NotImplementedError("only numpy ndarray circles handled")
    return True

def circle_centers(circles):
    """Given a sequence of circles, return a sequence of tuples representing XY coordinates of the circle centers."""
    centers = []
    for circle in circles[0,:]:
        point = (circle[0],circle[1])
        centers.append(point)
    return centers

def circles_array_to_list(circles):
    circlesList = []
    for i in circles[0,:]:
        circlesList.append(i)
    return circlesList

# FIXME: need to do sanity checks (e.g., if img is grayscale, will it be problematic to use blue or red as a color?)

# circles: an array of ...?
def draw_circles(img, circles):
    for i in circles[0,:]:
        # draw the outer circle
        cv2.circle(img, (i[0],i[1]), i[2], (0,255,0), 2)
        # draw the center of the circle
        cv2.circle(img, (i[0],i[1]), 2, (0,0,255), 3)

# circles: a list of circles (as returned by find_circles)

# use the actual radius of the circle unless radius is defined as an argument
def draw_circles_2(img, circles, color=None, line_thickness=3, radius=None):
    """Draw a series of circles."""
    # color defaults
    if not color:
        if grayscale_p(img):
            color = 0
        else:
            color = (255,0,0)
    for circle in circles:
        # Q: why does it need to be a numpy circle?
        if True: # numpyCircleP(circle):
            # OLD: fixed radius of 20: cv2.circle(img, (circle[0],circle[1]), 20, color, 3)
            working_radius = radius or circle[2]
            cv2.circle(img, (circle[0],circle[1]), working_radius, color, line_thickness)
        else:
            lg.error("!!! circle not a numpy circle")

# radius_scale allows us to specify a smaller (interior) circle of analysis within the specified circle (a value of 1 means to use the circle's radius)

# img should be a grayscale image

# return a single number
def mean_circle_interior_color(img,circle,radius_scale,debug_img):
    if not numpyCircleP(circle):
        lg.error("mean_circle_interior_color -- circle %s not a numpy circle")
    circle_mask = np.zeros(img.shape[:2], dtype="uint8")
    circle_radius = circle[2]
    roi_radius = int(round(radius_scale*circle[2]))
    new_circle = (circle[0],circle[1],roi_radius)
    # diagnostic image
    debug_circles=[new_circle]
    debug_line_color1 = (255,0,0)
    debug_line_color2 = (0,0,255)
    debug_line_color3 = (0,255,0)
    debug_line_thickness = 2
    cv2.line(debug_img, (0,0), (circle[0],circle[1]), debug_line_color1, debug_line_thickness)
    draw_rectangle_around_circle (debug_img,new_circle,debug_line_color2,debug_line_thickness)
    draw_circles_2(debug_img,debug_circles, color=debug_line_color3, line_thickness=debug_line_thickness)
    # is this *filling* the circle?
    cv2.circle(circle_mask, (circle[0],circle[1]), roi_radius, 255, -1)
    means = cv2.mean(img, mask = circle_mask)
    return means[0]

# a circle object is a numpy ndarray where data is a tuple (x,y,radius);
def numpyCircleP(x):
    # simpleCircleP
    # if isinstance(x,list) and len(x)==3:
    if isinstance(x,np.ndarray):
        if x.shape == (3,):
            return True
        else:
            return False
    else:
        return False
    return True

#
# rectangles
def draw_rectangle_around_circle (img,circle,color,line_thickness):
    #color = 0
    #line_thickness = 3
    circle_center_x = circle[0]
    circle_center_y = circle[1]
    circle_r = circle[2]
    rect_pt1 = (int(circle_center_x-circle_r),int(circle_center_y-circle_r))
    rect_pt2 = (int(circle_center_x+circle_r),int(circle_center_y+circle_r))
    cv2.rectangle(img, rect_pt1, rect_pt2, color, line_thickness)

def rectangleP(x):
    if isinstance(x, list) and len(x) == 4:
        return True
    else:
        return False

#
# lines

# each member of POINTS has the form (88.60952, 88.60952)
def lines_from_point(origin, points, img, color=(0,0,0)):
    """POINTS should be a sequence of tuples"""
    int_points = np.round(points).astype(int)
    for point in int_points:
        # line(img, pt1, pt2, color[, thickness[, lineType[, shift]]]) -> img
        cv2.line(img, origin, point, color, 5)

#
# images
def grayscale_p(img):
    """Return a boolean indicating whether the image IMG is grayscale."""
    first_pixel = img[0,0]
    if isinstance(first_pixel, int):
        return True
    elif isinstance(first_pixel, np.uint8):
        return True
    else:
        return False

def px_per_cm(img, x_cm, y_cm):
    """
    Return a floating point estimate of pixels per centimeter for
    image IMG given the asserted physical dimensions of the image,
    X_CM and Y_CM.
    """
    # IMG.SHAPE returns a tuple of 2 or 3 members depending on whether IMG is color or greyscale
    img_rows = img.shape[0]
    img_cols = img.shape[1]
    rows_per_cm = img_rows / y_cm
    cols_per_cm = img_cols / x_cm
    lg.debug("img_rows img_cols rows_per_cm cols_per_cm %s %s %s %s",
             img_rows,
             img_cols,
             rows_per_cm,
             cols_per_cm
             )
    # relative tolerance of 0.05 is a shot in the dark. The default is
    # 1e-05. 0.01 is too low from scans from some devices.
    assert np.allclose([round(rows_per_cm)], [round(cols_per_cm)], 0.05), "Scaling appears to be different for X and Y dimensions."
    best_guess = np.mean([rows_per_cm, cols_per_cm])
    lg.debug("best_guess %s",best_guess) # 59.07301360057265
    return best_guess

# cv2 imread doesn't complain if the file passed as argument doesn't exist
def sane_imread(file, flag):
    if os.path.isfile(file):
        return cv2.imread(file, flag)
    else:
        raise Exception('file does not exist:', file)

def sane_imwrite(file,img):
    if os.path.isfile(file):
        os.remove(file)
    return cv2.imwrite(file, img)

#
# points
def xy_arrays_to_xy_tuples(points_as_arrays):
    points_as_tuples=[]
    for point in points_as_arrays:
        point_as_tuple = (point[0],point[1])
        points_as_tuples.append(point_as_tuple)
    return points_as_tuples

#
# types

# type is a type object
def typeP (x,type):
    if type(x)==type:
        return True
    else:
        return False

#
# uncategorized

# test whether circle is in rectangle
# circle is a tuple (x,y,radius);
# rectangle is a tuple [ x1,y1,x2,y2] ] where x1 < x2 and y1 < y2
def circleInRectangleP (circle, rectangle, centerP=True):
    # sanity checks
    assert numpyCircleP(circle),'circle should be a numpy circle'
    assert rectangleP(rectangle),'rectangle should be a list with 4 elements'
    if centerP:
        return pointInRectangleP( circle[0], circle[1], rectangle )
    else:
        raise NotImplementedError("centerP=false not handled")

# FIXME: ensure containing algorithm uses this correctly
# note that 'in' doesn't include rectangle boundary itself
def pointInRectangleP (x, y, rectangle):
    if x < rectangle[2] and x > rectangle[0]:
        if y < rectangle[3] and y > rectangle[1]:
            return True
        else:
            return False
    else:
        return False

# FIXME?
# fundamentally, this could be simplified as findCornerPoint -- given a series of points, find the upper-left-most, upper-right-most, lower-left-most, or lower-right-most point

# centerP: if centerP is true, find upper-left-most circle where center of circle is within rectangle


# CIRCLES is of type <type 'numpy.ndarray'>
#   a numpy circle object: a numpy ndarray where data is a tuple (x,y,radius);
# RECTANGLE is a tuple [ x1,y1,x2,y2] ] where x1 < x2 and y1 < y2
# FACTOR is an internal variable (the algorithm uses upper left hand rectangle size dependent on factor)
# CORNER: 8 (upper-left), 4 (upper-right), 2 (lower-left), 1 (lower-right)
# RECURSE_COUNT: an integer; used internally to limit excessive recursion
def findCornerCircle (circles, rectangle, corner, centerP=True, error_p=True, factor=2, oldFactor=1, recurse_count=0):
    assert recurse_count<20
    assert isinstance(circles, np.ndarray)
    # sanity conversion for python's math madness with integer
    # division in python 2.x
    factor=float(factor)
    oldFactor=float(oldFactor)
    # begin by searching in rectangle anchored in the upper-left-hand corner; expand or contract rectangle as needed...
    xdelta=(rectangle[2]-rectangle[0])/factor
    ydelta=(rectangle[3]-rectangle[1])/factor
    if corner==8:# ul
        searchRectangle = [rectangle[0], rectangle[1], rectangle[0]+xdelta, rectangle[1]+ydelta]
    elif corner==4:# ur
        searchRectangle = [rectangle[0]+xdelta, rectangle[1], rectangle[2], rectangle[3]-ydelta]
    elif corner==2:# ll
        searchRectangle = [rectangle[0], rectangle[3]-ydelta, rectangle[0]+xdelta, rectangle[3]]
    elif corner==1:# lr
        searchRectangle = [rectangle[0]+xdelta, rectangle[1]+ydelta, rectangle[2], rectangle[3]]
    # new circles of interest
    circleSet=[]
    for circle in circles:
        if circleInRectangleP(circle, searchRectangle, centerP):
            circleSet.append(circle)
    # give up if we apparently have searched virtually the whole rectangle and nothing shows up...
    if (factor-1.0) < 0.01:
        return []
    # handle different circleSet possibilities
    if len(circleSet)==0:
        # make search rectangle bigger
        newFactor=(oldFactor+factor)/2
        finalCircle=findUpperLeftMostCircle(circles, rectangle, True, newFactor, oldFactor, recurse_count+1)
    elif len(circleSet)==1:
        finalCircle=circleSet[0]
    else:
        finalCircle=findUpperLeftMostCircle(circleSet, searchRectangle, True, 2, 1, recurse_count+1)
    assert numpyCircleP(finalCircle), "Couldn't find circle"
    return finalCircle

def findUpperLeftMostCircle(circles,rectangle,centerP=True,factor=2,oldFactor=1,recurse_count=0):
    findCornerCircle(circles, rectangle, 8, centerP, factor, oldFactor, recurse_count)
