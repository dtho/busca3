#
# exception classes
#
class CalibrationMarkNotDetected(Exception):
    '''Raise when at least one calibration mark is not detected'''
    def __init__(self, message, diagnostic_file, additional_notes, *args):
        self.message = message # without this you may get DeprecationWarning
        # location of diagnostic file
        self.diagnostic_file = diagnostic_file
        # array of strings
        self.additional_notes = additional_notes
        # *args allows users to initialize other arguments -- any additional arguments should be strings representing additional comments/notes/...
        super(CalibrationMarkNotDetected, self).__init__(message, diagnostic_file, additional_notes, *args)
