# -*- coding: utf-8 -*-

#
# pdf.py
#
#   1. accepts an input file (PDF) 
#   2. generates a set of PNG files for the pages in the input file
#

# Modules
import io
import os
import subprocess
import PyPDF2

from wand.image import Image

# Logging
import logging
# define busca logger as the generic logger
lg=logging

# Internal modules
from busca3 import json1

def pdf_to_pngs(pdf_file):  
    """
    Generate PNG files, one corresponding to each page of the PDF file pdf_file. Return a list of the PNG file names.
    """
    input_file_sans_suffix, input_file_suffix = os.path.splitext(pdf_file)
    outfile_root=input_file_sans_suffix
    #
    # determine number of pages
    x = PyPDF2.PdfReader(pdf_file) # https://pypdf2.readthedocs.io/en/latest/modules/PdfReader.html
    numberofpages = len(x.pages) # x.getNumPages()
    lg.debug("numberofpages: %s",numberofpages)
    # See pdf_to_pngs-pdf.py-busca3-NOTES.org for considerations on implementation
    lg.info("converting PDF to PNG images... this may consume some time")
    # FIXME: ignore page(s) here -- don't wait
    for page_number in range(1,numberofpages+1):
        #lg.info(f'{page_number} {pdf_file} {outfile_root}')
        # See busca3/doc/pdftoppm.org
        returncode = subprocess.call(
            ["pdftoppm", "-f", str(page_number), "-l", str(page_number), "-gray", "-png", pdf_file, outfile_root],
            shell=False)
        if (returncode == 0):
            lg.info(json1.json_completed_pdf_to_ppm([page_number],numberofpages))
        else:
            lg.error(json1.json_failed_to_convert_pdf(None,pdf_file))
    # return file names
    png_files=[]
    # Given the limitations imposed by pdftoppm,
    # plan ahead for the file names
    index_format_string = ""
    if ( numberofpages < 10 ):
        index_format_string = "{1:d}"
    elif ( numberofpages < 100 ):
        index_format_string = "{1:0>02d}"
    elif ( numberofpages < 1000 ):
        index_format_string = "{1:0>03d}"
    else:
        raise Exception('no support (at this point) for page count exceeding 1000 pages')
    string_format_string = "{0}-" + index_format_string + ".png"
    for pagenumber in range(numberofpages):
        png_infile = str.format(
            string_format_string, # "{0}-{1:d}.png"
            outfile_root,pagenumber+1); 
        png_files.append(png_infile) 
    lg.debug("png_files length: %s",len(png_files))
    return png_files

# Yet to implement:
#     
#     End at [ some page close to ] END_PAGE.
#     

def pdf_to_pngs_generator(pdf_file,n,
                          skip_p,
                          begin_page=1,
                          skip_forward=0):
    """Return a generator. Each call to the generator generates N PNG
    files, each file corresponding to one page of the PDF document specified by PDF_FILE. Generation begins at BEGIN_PAGE (an integer). Page counting begins with one. Each call to the generator returns a tuple where the first value is a list of the corresponding file names and the second and third values are the first and last page numbers for the pages used to generate the PNG images specified in the first value. SKIP_FORWARD, an integer, indicates the number of pages to "skip" after the N PNGs have been generated. For example, if BEGIN_PAGE is 1, N is 2 and SKIP_FORWARD is 3, the first four PNG files in the return value will correspond to pages 1, 2, 6, and 7 in the PDF document under consideration.
    """
    lg.debug("PDF_TO_PNGS_GENERATOR.00 %s %s",pdf_file,n)
    input_file_sans_suffix, input_file_suffix = os.path.splitext(pdf_file)
    outfile_root=input_file_sans_suffix
    # determine number of pages
    x = PyPDF2.PdfReader(pdf_file) # https://pypdf2.readthedocs.io/en/latest/modules/PdfReader.html
    numberofpages = len(x.pages) # x.getNumPages()
    lg.debug("PDF_TO_PNGS_GENERATOR.30 %s",numberofpages)
    # see old function for notes on pain associated with using pdftoppm
    # pdftoppm starts page numbering at 1
    first_page_number = begin_page
    while first_page_number <= numberofpages:
        # LAST_PAGE_NUMBER specifies the last page in the current set of N
        last_page_number = first_page_number+n-1
        #print("pages: " + str(first_page_number) + " " + str(last_page_number))
        returncode = subprocess.call(
            ["pdftoppm", "-f", str(first_page_number), "-l", str(last_page_number), "-gray", "-png", pdf_file, outfile_root], shell=False)
        if (returncode == 0):
            lg.info(f'{first_page_number} {last_page_number}')
            lg.info(json1.json_completed_pdf_to_ppm(list(range(first_page_number,
                                                               last_page_number+1)),
                                                    numberofpages))
        else:
            lg.error(json1.json_failed_to_convert_pdf(None,pdf_file))
        # return file names
        png_files=[]
        # Due to the limitations of pdftoppm, we must plan ahead for the file names, anticipating pdftoppm's default non-configurable behavior.
        index_format_string = ""
        if ( numberofpages < 10 ):
            index_format_string = "{1:d}"
        elif ( numberofpages < 100 ):
            index_format_string = "{1:0>02d}"
        elif ( numberofpages < 1000 ):
            index_format_string = "{1:0>03d}"
        else:
            raise Exception('no support (at this point) for page count exceeding 1000 pages')
        # "{0}-{1:d}.png",
        string_format_string = "{0}-" + index_format_string + ".png"
        for pagenumber in range(first_page_number,last_page_number+1):
            png_infile = str.format(string_format_string, outfile_root,pagenumber);
            png_files.append(png_infile)
        yield png_files, first_page_number, last_page_number
        first_page_number=first_page_number+n+skip_forward
