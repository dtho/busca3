# -*- coding: utf-8 -*-

# (c) David A. Thompson <thompdump@gmail.com>
#
# This file is part of Busca
#
# Busca is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Busca is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Busca. If not, see <http://www.gnu.org/licenses/>.


#
# deskew.py accepts an input file (png) and
#     - locates calibration marks edges/corners
#     - generates a deskewed image -- an adjusted image where the calibration and bubble marks are, in theory, located where they are "supposed to be"
#     - with the deskewed version, locates:
#         - ID box (if present)
#         - bubble marks
#     - outputs
#         A. an PNG file with the deskewed image
#         B. to stdout, the absolute path of the deskewed image file
#

# Modules
import cv2
import numpy
import getopt
import os.path
import sys

# Logging
import logging
# define deskew logger
lg=logging

# Internal modules
from busca3 import exceptions
from busca3 import feature_detect
from busca3 import json1
from busca3 import reference_image
from busca3 import util

# debugp
deskew_debugp=False

#
# functions
#
def deskew_img_file(img_file, debugp):
    """
    From the PNG image specified by IMG_FILE (a string), generate a
    deskewed image file. Return, as a string, the output file path.
    Return None if an [apparently] empty page was encountered.
    """
    input_file_sans_suffix, input_file_suffix = os.path.splitext(img_file)
    diag_output_file = None
    diag_output_file_suffix = None
    # IMGRAY: a matrix (numpy.ndarray)
    imgray = util.sane_imread(img_file, cv2.IMREAD_GRAYSCALE)
    debug_img = None
    debugp = debugp or deskew_debugp
    if debugp:
        lg.debug(imgray)
        # diagnostic output
        diag_output_file_suffix='png'
        diag_output_file = input_file_sans_suffix + "DSK-DEBUG01" + "." + diag_output_file_suffix
        # - best to have it in color so it's easy to distinguish lines or other diagnostic marks
        debug_img = cv2.cvtColor(imgray,cv2.COLOR_GRAY2BGR)
        cv2.rectangle(debug_img, pt1=(200,200), pt2=(300,300), color=(0,0,255), thickness=10)
    #
    # circle detection
    #

    # discourage false circle detection
    cimg = cv2.GaussianBlur(imgray, (0,0), 2)
    # radii of calibration marks: 0.5cm
    px_per_cm = util.px_per_cm(imgray,
                               reference_image.ref_img_x_cm,
                               reference_image.ref_img_y_cm)
    cal_mark_radius_cm = 0.5
    min_radius_cm = 0.7*cal_mark_radius_cm
    max_radius_cm = 0.6
    min_radius_px = min_radius_cm * px_per_cm
    max_radius_px = max_radius_cm * px_per_cm
    # minimum distance
    # - by specifying a minimum distance, we can use 'fuzzier' circle detection and still limit probability of false positives
    min_dist_cm = reference_image.right_cal_mark_x_cm - reference_image.left_cal_mark_x_cm
    # allow for a bit of error
    min_dist_cm = 0.9 * min_dist_cm
    min_dist = min_dist_cm * px_per_cm
    # CIRCLES is of type <type 'numpy.ndarray'>
    circles = feature_detect.find_circles_in_corners01(imgray,min_radius_px,max_radius_px,min_dist,input_file_sans_suffix+"C-DEBUG","png")
    assert isinstance(circles,numpy.ndarray)
    if (debugp and len(circles)>0):
        lg.debug("DESKEW_IMG_FILE.55 %s",diag_output_file)
        util.draw_circles_2(debug_img,circles,radius=20)
        # imwrite chooses format based on filename extension
        cv2.imwrite(diag_output_file,debug_img)
    # possibilities: (1) good analysis, (2) bad analysis, or (3) looking at a blank page or seriously damaged page

    # attempt a sanity check -- circles should be solid black -- if the interior of one or more circles is mostly white, we know something went awry
    sc_output_file_suffix='png'
    sc_output_file = input_file_sans_suffix + "CIRCLE-INTERIORS" + "." + sc_output_file_suffix
    sc_img = util.sane_imread(img_file, cv2.IMREAD_GRAYSCALE)
    sc_img = cv2.cvtColor(sc_img,cv2.COLOR_GRAY2BGR)
    empty_circle_p = False
    # With grayscale, the circle may be a substantially lighter shade (at least as light as #424242). Approaches to identifying "filled-in" versus "white":
    # 1. use an absolute value as threshold (simplest, current implementation)
    # 2. use delta in values for cutoff (i.e., look for a substantial difference between area w/known-white background and area w/expected-black background)

    # 20 is too dark; some greyscale scans will have "filled-in" circles that are substantially lighter
    mostly_black = 60
    for circle in circles:
        mean_int_color = util.mean_circle_interior_color(imgray,circle,0.8,sc_img)
        lg.debug("DESKEW_IMG_FILE.60 mean_int_color %s mostly_black %s",mean_int_color,mostly_black)
        if mean_int_color > mostly_black:
            lg.debug("!! not mostly black!!")
            empty_circle_p = True
            break
    if debugp or empty_circle_p:
        util.sane_imwrite(sc_output_file,sc_img)
    if empty_circle_p:
        raise exceptions.CalibrationMarkNotDetected("at least one calibration circle is not black",sc_output_file,["The green circle indicates our best attempt to find a calibration mark in the area in which a mark was expected."])
    # FIXME: make blank page detection more sophisticated: it's entirely possible that page is blank and that a few random 'circles' were detected -- length of circles isn't sufficient for this --> should revisit algorithm for focusing only on corner rectangles for grabbing circles (write FIND_CIRCLES_CORNERS)
    # FIXME: clearly indicate which png image file was associated w/a blank or failure (page number in the original PDF would be even nicer...)
    # FIXME: gracefully fail and continue processing remaining pages...
    lg.debug(f'DESKEW_IMG_FILE.80 {circles}')
    if len(circles) <= 2:
        lg.info("encountered apparent blank page at %s",img_file)
        # ... we could do a further check to try and determine if page really looks blank... (is page blank or full of a bunch of stuff?)
        # Can we use mu/sigma of raw image?
        (mu, raw_sigma) = cv2.meanStdDev(imgray)
        sigma = raw_sigma[0][0]
        # blank: mu/sigma: [[ 254.88690053]] [[ 3.97884775]]
        # content: mu/sigma: [[ 248.96271135]] [[ 31.11935189]]
        lg.info("mu/sigma: %s %s %s",mu,sigma,raw_sigma)
        assert (sigma<20),"High sigma suggests page isn't blank; could not deskew image at "+img_file
        return None
    else:
        # sanity check
        assert len(circles)>3,"Calibration marks not found; could not deskew image at "+img_file
        lg.debug(f'diag_output_file {diag_output_file} img_file {img_file} imgray {imgray}')
        return deskew_img_file_good_page(cimg,circles,debug_img,debugp,diag_output_file,diag_output_file_suffix,img_file,imgray,input_file_sans_suffix)

def deskew_img_file_good_page (cimg, circles,
                               debug_img, debugp,
                               diag_output_file, diag_output_file_suffix,
                               img_file, imgray, input_file_sans_suffix):
    """Return either the absolute path of a file or None."""
    lg.debug(f'imgray {imgray.shape[0]} {imgray.shape[1]} debugp {debugp}')
    if debugp:
        lg.debug("debug_img: %s %s", debug_img.shape[0], debug_img.shape[1])
    # locate calibration points (locate_calibration_marks performs sanity check)
    corner_circles = locate_calibration_marks(imgray,circles)
    if debugp:
        util.draw_circles_2(debug_img,corner_circles,(300,300),radius=20)
        diag_output_file = input_file_sans_suffix + "DSK-DEBUG02" + "." + diag_output_file_suffix
        cv2.imwrite(diag_output_file,debug_img)
        cv2.imwrite(input_file_sans_suffix+"0001"+".png",imgray)
    # deskew
    deskewed_img = deskew_using_cal_marks(corner_circles,imgray,debug_img)
    # write out image file
    output_file_suffix='png'
    input_file_sans_suffix, input_file_suffix = os.path.splitext(img_file)
    outputFile = input_file_sans_suffix + "DESKEW" + "." + output_file_suffix
    imwrite_params = list()
    # The IMWRITE_PNG_COMPRESSION default is 16.
    # The documentation, at one point, indicated that a range from 0 to 9 is acceptable and that 3 is the default. 9 will result in smaller files but longer compression time.
    imwrite_params.append(cv2.IMWRITE_PNG_COMPRESSION)
    imwrite_params.append(9)
    # imwrite chooses format based on filename extension
    cv2.imwrite(outputFile,deskewed_img,imwrite_params)
    # send absolute path of output file to stdout
    abs_path = os.path.abspath(outputFile)
    lg.debug("outputFile: %s %s",outputFile,abs_path)
    return abs_path

def deskew_using_cal_marks(
        corner_circles,
        img,
        debug_img):
    """
    Return a deskewed version of IMG. Unless DEBUG_IMG is None, mark
    DEBUG_IMG to indicate where actual and ideal reference points are.
    """
    ulCircle=corner_circles[0]
    urCircle=corner_circles[1]
    llCircle=corner_circles[2]
    lrCircle=corner_circles[3]
    lg.debug("corner_circles %s",corner_circles)
    # actual reference point locations in image (ul=upper-left, etc.)
    actualPoints = numpy.float32( [
        [ ulCircle[0], ulCircle[1] ],
        [ urCircle[0], urCircle[1] ],
        [ llCircle[0], llCircle[1] ],
        [ lrCircle[0], lrCircle[1] ]] )
    # ideal reference point locations (circle centers as xy tuples)
    # if img is color image: img_rows,img_cols,imgCh = img.shape
    img_rows,img_cols = img.shape
    # floating point estimate
    px_per_cm = util.px_per_cm(img,
                               reference_image.ref_img_x_cm,
                               reference_image.ref_img_y_cm)
    upper_ideal_y = reference_image.upper_cal_mark_y_cm * px_per_cm
    lower_ideal_y = reference_image.lower_cal_mark_y_cm * px_per_cm
    left_ideal_x = reference_image.left_cal_mark_x_cm * px_per_cm
    right_ideal_x = reference_image.right_cal_mark_x_cm * px_per_cm
    lg.debug("upper_ideal_y … %s %s %s %s",
             upper_ideal_y,
             lower_ideal_y,
             left_ideal_x,
             right_ideal_x)
    referencePoints = numpy.float32([
            [ left_ideal_x, upper_ideal_y ],
            [ right_ideal_x, upper_ideal_y ],
            [ left_ideal_x, lower_ideal_y ],
            [ right_ideal_x, lower_ideal_y ]
            ])
    # debug
    referencePoints_as_tuples = util.xy_arrays_to_xy_tuples(referencePoints)
    if ( debug_img is not None):
        util.lines_from_point((400,400),
                              referencePoints_as_tuples,
                              debug_img,
                              (0,0,255))
    # define output image dimensions
    output_rows_float = reference_image.ref_img_y_cm * px_per_cm
    output_cols_float = reference_image.ref_img_x_cm * px_per_cm
    output_rows = int(output_rows_float)
    output_cols = int(output_cols_float)
    lg.debug("actualPoints,referencePoints %s %s",
             actualPoints,referencePoints)
    return deskew_with_perspective(img,actualPoints,referencePoints,output_cols,output_rows)

# return output image of the same type as src
def deskew_with_affine(img, actualPoints, referencePoints, output_x, output_y):
    lg.debug("%s",dir(img))
    affTransform = cv2.getAffineTransform(actualPoints, referencePoints)
    # necessary to create image of same dimensions?
    output_img = numpy.zeros((output_y,output_x,3), numpy.uint8)
    cv2.warpAffine(img,affTransform,(output_x,output_y),output_img)
    return output_img

# return output image of the same type as src
def deskew_with_perspective(img, actualPoints, referencePoints,
                            output_x, output_y):
    transform = cv2.getPerspectiveTransform(actualPoints, referencePoints)

    # debugging
    #cv2.imwrite("/home/thomp/fpu/classes/BIOL111--biology/tests/2014Fall/quiz01--2014-08-26/0002.png",img)

    # necessary to create image of same dimensions?
    #output_color_img = numpy.zeros((output_y,output_x,3), numpy.uint8)
    output_gray_img = numpy.zeros((output_y,output_x),numpy.uint8)
    cv2.warpPerspective(img,transform,(output_x,output_y),output_gray_img)
    return output_gray_img

def edge_detection(imgray):
    harris_dst = edge_detection(imgray)
    # color the detected regions at threshold (0.1 is pretty picky...)
    threshold = 0.1
    # draw red lines in image
    img[harris_dst>threshold*harris_dst.max()]=[0,0,255]

# return an array of circles [ ulCircle, urCircle, llCircle, lrCircle ]
def locate_calibration_marks(imgray, circles, errorp=True):
    '''Find calibration marks (corner circles). CIRCLES is of type
    <type 'numpy.ndarray'>. IMGRAY is a grayscale image as returned by
    cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)'''
    lg.debug("LOCATE_CALIBRATION_MARKS.00: circles: %s %s",circles,imgray)
    pixelsWide = imgray.shape[1]
    pixelsHigh = imgray.shape[0]
    # RECTANGLE [x1,y1,x2,y2] specifies search area via two xy coordinates
    rectangle = [0,0,pixelsWide-1,pixelsHigh-1]
    ulCircle = util.findCornerCircle(circles,rectangle,8,True)
    urCircle = util.findCornerCircle(circles,rectangle,4,True)
    llCircle = util.findCornerCircle(circles,rectangle,2,True)
    lrCircle = util.findCornerCircle(circles,rectangle,1,True)
    if errorp:
        assert util.numpyCircleP(ulCircle), "Failed to find calibration mark"
        assert util.numpyCircleP(urCircle), "Failed to find calibration mark"
        assert util.numpyCircleP(llCircle), "Failed to find calibration mark"
        assert util.numpyCircleP(lrCircle), "Failed to find calibration mark"
    cornerCircles = [ ulCircle, urCircle, llCircle, lrCircle ]
    return cornerCircles

# detect edges/corners
def edge_detection(imgray):
    # Harris edge detector; puts Harris detector responses in output array harris_dst
    blockSize=2
    apertureSize=3
    k=0.04
    harris_dst = cv2.cornerHarris(imgray,blockSize,apertureSize,k)
    harris_dst = cv2.dilate(harris_dst,None)
    return harris_dst

def parse_cli_args ():
    # get input (PDF) filename from commandline
    # - expect it to be the last argument
    # returns a tuple of arrays
    cliargs = getopt.getopt(sys.argv[1:],"gt")
    inputFile = cliargs[1][-1]
    # 2480 3508
    # inputFile = "/home/thomp/computing/javascript/src/dat-ocr/corners01.png"
    # outputFile = "/home/thomp/computing/javascript/src/dat-ocr/cornersOUT01.png"
    return inputFile

def main():
    inputFile = parse_cli_args()
    deskew_img_file(inputFile,False)


########################################################

# main()
